const Event = use('Event')
const Mail = use('Mail')

Event.on('new::user', async (user) => {  

	const halaman = `<div style="padding: 10px;">
		<div style="padding: 10px; text-align:center; border-radius:5px; border: 1px solid #ccc;">
			<h1>Animeflix.id</h1>
			<b>Selamat Datang Di Animeflix.id</b>
			<hr>
			<p>Silahkan klik tombol aktivasi dibawah untuk mengaktifkan akun anda</p>
			<br>
			<a href=http://animedemy.me:4142/api/v1/activate/${user.activate_code} style="padding: 10px;background-color: #09f;color: #fff; border-radius:5px">
			Aktifasi Akun
			</a>
			<br>
			<br>
			<p>Kode Aktivasi Anda = ${user.activate_code}</p>
		<div>
	<div>`

	await Mail.raw(halaman, (message) => {
		message
			.to(user.email)
			.from('animeflix.id@gmail.com')
			.subject('Activated Code')
	})
})