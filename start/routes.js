'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.group(() => {
  //Auth    
  Route.get('logout','AuthController.logout').as('logout').middleware(['auth'])
  Route.get('activate/:activate_code','AuthController.activate').as('activate')

  Route.post('login', 'AuthController.index').as('login')
  Route.post('register','AuthController.create').as('register')
  Route.post('isloggedin','AuthController.isLoggedin').as('isLoggedin').middleware(['auth'])

  //User
  Route.get('user/profile','UserController.show').as('profile').middleware(['auth:jwt'])
  Route.get('user/favorites','UserController.getFavorite').as('getFavorites').middleware(['auth:jwt'])
  Route.put('user/changepassword','UserController.changePassword').as('Change Password').middleware(['auth'])
  Route.post('user/favorite','UserController.favorites').as('favorites').middleware(['auth:jwt'])
  Route.delete('user/favorite/:id','UserController.destroy').as('deleteFavorites').middleware(['auth:jwt'])

  //Videos
  Route.get('videos/:offset/:limit', 'VideoController.index').as('getAllVideo')
  Route.get('videos/cached', 'VideoController.cache').as('getVideoCache')
  Route.get('videos/cached/featured/:limit', 'VideoController.cacheFeatured').as('getVideoCacheFeatured')
  Route.get('videos/cached/trending/:limit', 'VideoController.cacheTrending').as('getVideoCacheTrending')
  Route.get('videos/cached/popular/:limit', 'VideoController.cachePopular').as('getVideoCachePopular')
  //Route.get('video/:id', 'VideoController.show').as('showVideo')
  Route.get('videos/popular/:limit', 'VideoController.popular').as('getPopuler')
  Route.get('videos/trending/:limit', 'VideoController.trending').as('getTrending')
  Route.get('videos/search', 'VideoController.search').as('search video')
  Route.get('videos/categories', 'VideoController.allCategory').as('categories')
  Route.get('videos/:category/:limit', 'VideoController.videoCategory').as('VideoCategory')
  Route.get('video/:slug', 'VideoController.showSlug').as('showSlug')
  Route.get('video/slug/:id', 'VideoController.addSlug').as('addSlug')
  Route.get('video/series/popular/:limit', 'VideoController.showSeriesPopular').as('series popular')
  Route.get('video/series/:series/:limit', 'VideoController.showVideoBySeries').as('video series')  

  //Series
  Route.get('series/search', 'SeriesController.search').as('search series')

  //Category
  Route.get('video/:category/:limit', 'CategoryController.show').as('category video')  
}).prefix('api/v1')