'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SeriesSchema extends Schema {
  up () {
    this.create('series', (table) => {
      table.increments()
      table.string('series', 254).notNullable().unique()
      table.string('rating', 20).notNullable().unique()
      table.longtext('description').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('series')
  }
}

module.exports = SeriesSchema
