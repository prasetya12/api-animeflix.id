'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FavouriteSchema extends Schema {
  up () {
    this.create('favourites', (table) => {
      table.increments()
      table.string('id_user',10).notNullable()
      table.string('name_series',10).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('favourites')
  }
}

module.exports = FavouriteSchema
