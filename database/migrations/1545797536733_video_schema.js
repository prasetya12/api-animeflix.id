'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VideoSchema extends Schema {
  up () {
    this.create('videos', (table) => {
      table.increments()
      table.string('title')
      table.string('series')
      table.string('rating')
      table.string('episode')
      table.string('category')
      table.longtext('description')
      table.longtext('image_url')
      table.longtext('video_url')
      table.text('slug')
      table.timestamps()
    })
  }

  down () {
    this.drop('videos')
  }
}

module.exports = VideoSchema
