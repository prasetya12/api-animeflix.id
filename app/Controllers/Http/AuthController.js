'use strict'

const Muser = use('App/Models/User') 
const { validate } = use('Validator')
const Event = use('Event')
const Encryption = use('Encryption')
const Database = use('Database')

class AuthController {

  async index ({ request, response,auth}) {
      try{
          const rules = {
              email:'required',
              password:'required'
          }

          const validation = await validate(request.all(),rules)

          if(validation.fails()){
              response.status(400).json(validate.messages())
          }else{
              const { email, password } = request.all()
              const verifEmail = await Muser.findBy('email',email)
              const data = await auth.authenticator('jwt').withRefreshToken().attempt(email,password)
              const hasil = `{"type":"${data.type}","token":"${data.token}","refresh_token":"${data.refreshToken}","verifEmail":"${verifEmail.activate_status}"}`
              response.json(hasil)
          }
      }catch(e){
          response.json({success:false,message:e.message})
      }
  }

  async create ({ request, response}) {
      try{
          const result = await Muser.findBy('email', request.input('email'))

          if( result ){
              response.status(400).json({success:false,message:'Email sudah terdaftar'})
          }else{
              const rules = {
                  name:'required',
                  username: 'required|unique:users,username',
                  email: 'required|email|unique:users,email',
                  password: 'required'
              }

              const validation = await validate(request.all(), rules)

              if( validation.fails() ){
                  response.status(400).json({success:false,message:'Username has been used'})
              }else{
                  const activate_code = Encryption.encrypt(request.only('email').email+'/'+request.only('password').password).replace("/",".")
                  const user = {
                        email: request.only('email').email,
                        activate_code: activate_code
                  }

									Event.fire('new::user', user)
									const data_insert = request.all()
									const data_user = new Muser()
									data_user.username = data_insert.username
									data_user.email = data_insert.email
									data_user.name = data_insert.name
									data_user.password = data_insert.password
									data_user.app_id = data_insert.app_id
									data_user.activate_code = activate_code
									data_user.activate_status = '0'
									await data_user.save()

									response.json({success:true,message:'Register Success'})
              }
          }
      }catch(e){
          response.json({success:false,message:e.message})
      }
  }

//   async create ({ request, response}) {
//       try{
//           const result = await Muser.findBy('email', request.input('email'))

//           if( result ){
//               response.status(400).json({success:false,message:'Email sudah terdaftar'})
//           }else{
//               const rules = {
//                   name:'required',
//                   username: 'required|unique:users,username',
//                   email: 'required|email|unique:users,email',
//                   password: 'required'
//               }

//               const validation = await validate(request.all(), rules)

//               if( validation.fails() ){
//                   response.status(400).json({success:false,message:'Username has been used'})
//               }else{
//                   const regisUser = await Muser.create(request.all())
//                   Event.fire('new::user', result)
//                   response.json({success:true,message:'Register Success'})
//               }
//           }
//       }catch(e){
//           response.json({success:false,message:e.message})
//       }
//   }

	async activate({params, response, view}){
		const activate_code = params.activate_code
		const user = await Database.from('users').where('activate_code', activate_code).update({activate_status: '1'})
		return view.render('activation_code')
	}

  async logout ({ request,response,auth }) {
      const apiToken = auth.getAuthHeader()
      await auth.authenticator('jwt').revokeTokens([apiToken])
      response.json({success:true,message:"Logout success"})
  }


  async isLoggedin ({ auth, request, response}) {
      try{
            const token = await auth.check()
            const refreshToken = request.input('refresh_token')
            const newToken = await auth.generateForRefreshToken(refreshToken)
            response.json(newToken)	
      }catch(e){
          response.json({success:false,message:e.message})
      }
  }
}

module.exports = AuthController
