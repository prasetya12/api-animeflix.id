'use strict'

const Muser = use('App/Models/User')
const Mvideo = use('App/Models/Video')
const Mfavourite = use('App/Models/Favourite')
const Database = use('Database')
const Hash = use('Hash')

class UserController {

    async show({ request,response,auth }){
        const result = await auth.getUser()
        response.json(result)
    }

    async changePassword({request,response,auth}){
        const result = await auth.user.toJSON()
        const verifPassword = await Hash.verify(request.input('last_password'), result.password)

        if(verifPassword){
            const newPassword = await Hash.make(request.input('new_password'))
            const user = await Database.table('users').where('id',result.id).update('password',newPassword)
            response.json({success:true,message:"Change password success"})
        }else{
            response.json({success:false,message:"Password miss match"})
        }
    }

    async favorites({request,response,auth}){
        try{
            const result = await auth.getUser()
            const favorite = new Mfavourite()
            favorite.name_series = request.input('series')
            favorite.user_id = result.id
            favorite.save()
            response.json({success:true,message:`Add favorite ${request.input('series')}`})
        }catch(e){
            response.json({success:false,message:e.message})
        }
    }

    async getFavorite({request,response,auth}){
        try{
            const user = await auth.getUser()
            const result = await Database.table('favourites').innerJoin('series','favourites.name_series','series.series')
            response.json(result)
        }catch(e){
            response.json({success:false,message:e.message})
        }
    }

    async destroy ({ params, request, response }) {
        try{
            const result = await Mfavourite.find(params.id)
            await result.delete()
            response.json({success:true,message:`Delete favorite Success`})
        }catch(e){
            response.json({success:false,message:e.message})
        }
    }
}

module.exports = UserController
