'use strict'

const Mseries = use('App/Models/Series')

class SeriesController {

  async index ({ request, response, view }) {
  }

  async create ({ request, response, view }) {
  }

  async store ({ request, response }) {
  }

  async search({request, response, params}){
    let q = request.input('q')
    let o = request.input('o') || 1
    const result = await Mseries.query().where('series', 'like', '%'+q+'%').offset(parseInt(o)).limit(10).fetch()
    response.json(result)
  }

  async show ({ params, request, response, view }) {
  }

  async edit ({ params, request, response, view }) {
  }

  async update ({ params, request, response }) {
  }

  async destroy ({ params, request, response }) {
  }
}

module.exports = SeriesController
