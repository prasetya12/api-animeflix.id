'use strict'

const Mvideo = use('App/Models/Video')
const Mseries = use('App/Models/Series')
const Database = use('Database')
const Redis = use('Redis')

class VideoController {

  async index ({ request, response,params }) {
    let o = params.offset || 1
    const result = await Mvideo.query().orderBy('id','desc').offset(parseInt(o)).limit(`${params.limit}`).fetch()
    response.json(result)

  }

  
  async popular ({ request, response , params }) {

    const result = await Mvideo.query().orderBy('rating','asc').limit(`${params.limit}`).fetch();
    response.json(result)

  }

  async trending({request,response,params}){
    
    const result = await Mvideo.query().where('rating', 'like', '%7,%').limit(`${params.limit}`).fetch();
    response.json(result)

  }

  async show({request, response, params}){
    
    const result = await Mvideo.findBy('id',params.id)
    response.json(result)

  }

  async search({request, response, params}){
    let q = request.input('q')
    let o = request.input('o') || 1
    const result = await Mvideo.query().where('series', 'like', '%'+q+'%').offset(parseInt(o)).limit(10).fetch()
    response.json(result)
  }

  async allCategory({request,response}){
    const result = await Mvideo.query().distinct('category').orderBy('category','asc').fetch()
    response.json(result)
  }

  async videoCategory({request,response,params}){
    const result = await Mvideo.query().where('category',`${params.category}`).limit(`${params.limit}`).fetch()
    response.json(result)
  }

  async cache({request, response,params}){

    const resultRedis = await Redis.get('videos')

    if(resultRedis){
        return JSON.parse(resultRedis)
    }

    let resultFeatured = await Mvideo.query().where('rating', 'like', '%8,%').limit(`${params.limit}`).fetch()
    resultFeatured = JSON.stringify(resultFeatured)
    // let resultPopular = await Mvideo.query().orderBy('views','asc').limit(`${params.limit}`).fetch()
    // resultPopular = JSON.stringify(resultPopular)
    let resultTrending = await Mvideo.query().where('rating', 'like', '%7,%').limit(`${params.limit}`).fetch()
    resultTrending = JSON.stringify(resultTrending)
    // const all = `{"featured":${resultFeatured},"popular":${resultPopular},"trending":${resultTrending}}`
    const all = `{"featured":${resultFeatured},"trending":${resultTrending}}`

    await Redis.set('videos',JSON.stringify(all),'EX',300000)   
    response.json(all)
  }

  async cacheFeatured({request,response,params}){
    const resultRedis = await Redis.get('featured')

    if(resultRedis){
        return JSON.parse(resultRedis)
    }

    let result = await Mvideo.query().where('rating', 'like', '%8,%').limit(`${params.limit}`).fetch()
    result = JSON.stringify(result)
    await Redis.set('featured',JSON.stringify(result),'EX',300000)   
    response.json(result)
  }

  async cacheTrending({request,response,params}){
    const resutlRedis = await Redis.get('trending')

    if(resutlRedis){
        return JSON.parse(resutlRedis)
    }

    let result = await Mvideo.query().where('rating', 'like', '%7,%').limit(`${params.limit}`).fetch()
    result = JSON.stringify(result)
    await Redis.set('trending',JSON.stringify(result),'EX',300000)   
    response.json(result)
  }

  async cachePopular({request,response,params}){
    const resultRedis = await Redis.get('popular')

    if(resultRedis){
        return JSON.parse(resultRedis)
    }

    let result = await Mvideo.query().orderBy('rating','asc').limit(`${params.limit}`).fetch()
    result = JSON.stringify(result)
    await Redis.set('popular',JSON.stringify(result),'EX',300000)   
    response.json(result)
  }

  async showSlug({request, response, params}){
    const result = await Mvideo.findBy('slug',params.slug)
    response.json(result)
  }

  async addSlug({request,response,params}){
    const video = await Mvideo.findBy('id',params.id)
    let slug = video.title.replace(/ /g,"-").replace('...','').replace('/','-').toLowerCase()
    const result = await Database.table('videos').where('id',params.id).update('slug',slug)
    response.json('berhasil')
  }

  async showSeriesPopular({request,response,params}){
    const result  = await Mseries.query().where('rating', '>', 8).orderBy('rating','desc').limit(`${params.limit}`).fetch()
    response.json(result)
  }
  async showVideoBySeries({request,response,params}){
    let param = decodeURIComponent(params.series);
    const result = await Mvideo.query().where('series',param).limit(`${params.limit}`).fetch()
    response.json(result)
  }
}

module.exports = VideoController
