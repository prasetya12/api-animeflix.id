'use strict'

const Mseries = use('App/Models/Series')

class CategoryController {

  async index ({ request, response, view }) {
  }
  async create ({ request, response, view }) {
  }

  async store ({ request, response }) {
  }

  async show ({ params, request, response, view }) {
    const result = await Mseries.query().where('category','like',`%${params.category}%`).limit(`${params.limit}`).fetch()
    response.json(result)
  }

  async edit ({ params, request, response, view }) {
  }

  async update ({ params, request, response }) {
  }

  async destroy ({ params, request, response }) {
  }
}

module.exports = CategoryController
